import Model.Evaluate;
import Model.RetrievelResults;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

public class TestQueries {
	public static ArrayList<String> listQuery = new ArrayList<String>();

	public static void main(String args[]) throws IOException {
		ArrayList<String> listQuery = SearchDemo.GetQueryData();
		ArrayList<ArrayList<Map.Entry<String, Double>>> resultList = new ArrayList<ArrayList<Map.Entry<String, Double>>>();
		int count = 0;
		float MAP = 0;

		System.out.println("Writting Results");
		long startTime = System.currentTimeMillis();
		for (String query : listQuery) {
			System.out.print(".");
			count++;
			resultList.add(SearchDemo.getQuery(query));
			BufferedWriter FileWriter = new BufferedWriter(
					new FileWriter("./AllQueryResult/" + Integer.toString(count) + ".txt"));
			for (int i = 0; i < resultList.get(count - 1).size(); i++) {
				String[] fileName = resultList.get(count - 1).get(i).getKey().split(".txt");
				FileWriter.write(fileName[0] + "\n");
			}
			FileWriter.close();
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("\nFinished writting results to ./AllQueryResult !");
		System.out.println("Run time: " + (float) totalTime / 1000 + " s");

		System.out.println(".\nCalculating MAP");
		for (int i = 0; i < count; i++) {
			System.out.print(".");
			String testResult = "./DEV-TEST/RES/" + Integer.toString(i + 1) + ".txt";
			Evaluate.Calculate(resultList.get(i), testResult);
		}
		System.out.println("\nFinished calculating MAP!");
		MAP = Evaluate.GetMAP();
		System.out.printf("MAP: %f (%.2f%s)", MAP, MAP * 100, "%");
	}
}