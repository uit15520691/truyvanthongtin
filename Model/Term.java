package Model;

import java.util.ArrayList;

public class Term {
    private String WordId;
    private int NumDoc;
    private double Frequency, Idf;
    private ArrayList<PostingList> Posting;

    public Term(String word, int numDoc, double frequency, double idf){
        this.WordId = word;
        this.NumDoc = numDoc;
        this.Frequency = frequency;
        this.Idf = idf;
        this.Posting = new ArrayList<>();
    }

    public String getWordId() {
        return WordId;
    }

    public void setWordId(String WordId) {
        this.WordId = WordId;
    }
    
    
    public int getNumDoc() {
        return NumDoc;
    }

    public void setNumDoc(int NumDoc) {
        this.NumDoc = NumDoc;
    }


    public double getFrequency() {
        return Frequency;
    }

    public void setFrequency(double Frequency) {
        this.Frequency = Frequency;
    }


    public double getIdf() {
        return Idf;
    }

    public void setIdf(double Idf) {
        this.Idf = Idf;
    }

    public ArrayList<PostingList> getPosting() {
        return Posting;
    }
    
    public void AddPosting(String d, double f, double w){
        PostingList p = new PostingList(d, f, w);
        Posting.add(p);
    }
}
